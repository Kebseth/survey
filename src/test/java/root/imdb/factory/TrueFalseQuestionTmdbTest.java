package root.imdb.factory;

import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import root.beans.Counter;
import root.imdb.ReaderFakes;
import root.questionnaire.TrueFalseQuestion;

import java.time.Year;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TrueFalseQuestionTmdbTest {
    TmdbClient            mockTmdb;
    QuestionFactory       qf;
    MovieInfo             jokerInfo;
    TrueFalseQuestionTmdb tf;
    ReaderFakes           readerFakes;
    ConditionFactory      condition;
    Counter               counter;
    String                id;

   /* @BeforeEach
    void setUp(){
        id = "1";
        qf = new QuestionFactory(mockTmdb);
        condition = new ConditionFactory();
        readerFakes = new ReaderFakes();
        mockTmdb = mock(TmdbClient.class);
        jokerInfo = new MovieInfo("Joker", Year.of(2019),
                List.of(new MovieInfo.Character("Arthur Fleck / Joker", "Joaquin Phoenix" ),
                        new MovieInfo.Character( "Murray Franklin", "Robert De Niro"),
                        new MovieInfo.Character( "Sophie Dumond", "Zazie Beetz"),
                        new MovieInfo.Character( "Penny Fleck", "Frances Conroy")));
        tf = new TrueFalseQuestionTmdb(readerFakes, condition, counter);
        when(mockTmdb.getMovie("Joker")).thenReturn(Optional.of(jokerInfo));
    }

    @Test
    void isGoodYearWork() {
        Optional<MovieInfo> movieInfo = mockTmdb.getMovie("Joker");
        TrueFalseQuestion tfq = tf.goodYear(movieInfo.get());
        Assertions.assertEquals(tfq.getAnswer(), true);
    }

    @Test
    void isWrongYearWork() {
        Optional<MovieInfo> movieInfo = mockTmdb.getMovie("Joker");
        TrueFalseQuestion tfq = tf.wrongYear(movieInfo.get());
        Assertions.assertEquals(tfq.getAnswer(), false);
    }

    @Test
    void isGoodActorWork() {
        Optional<MovieInfo> movieInfo = mockTmdb.getMovie("Joker");
        TrueFalseQuestion tfq = tf.goodActor(movieInfo.get());
        Assertions.assertEquals(tfq.getAnswer(), true);
    }

    @Test
    void isWrongActorWork() {
        Optional<MovieInfo> movieInfo = mockTmdb.getMovie("Joker");
        TrueFalseQuestion tfq = tf.wrongActor(movieInfo.get());
        Assertions.assertEquals(tfq.getAnswer(), false);
    }

    @Test
    void isGoodMovieWork() {
        Optional<MovieInfo> movieInfo = mockTmdb.getMovie("Joker");
        TrueFalseQuestion tfq = tf.goodMovie(movieInfo.get());
        Assertions.assertEquals(tfq.getAnswer(), true);
    }

    @Test
    void isWrongMovieWork() {
        Optional<MovieInfo> movieInfo = mockTmdb.getMovie("Joker");
        TrueFalseQuestion tfq = tf.wrongMovie(movieInfo.get());
        Assertions.assertEquals(tfq.getAnswer(), false);
    }
*/
}
