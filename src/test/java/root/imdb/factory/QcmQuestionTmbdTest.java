package root.imdb.factory;

import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import root.beans.Counter;
import root.imdb.ReaderFakes;
import root.questionnaire.QcmQuestion;

import java.time.Year;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class QcmQuestionTmbdTest {
    TmdbClient       mockTmdb;
    QuestionFactory  qf;
    MovieInfo        jokerInfo;
    QcmQuestionTmdb  qcm;
    ReaderFakes      readerFakes;
    ConditionFactory condition;
    Counter          counter;
    String           id;

 /*   @BeforeEach
    void setUp(){
        id = "1";
        qf = new QuestionFactory(mockTmdb);
        condition = new ConditionFactory();
        readerFakes = new ReaderFakes();
        mockTmdb = mock(TmdbClient.class);
        jokerInfo = new MovieInfo("Joker", Year.of(2019),
                List.of(new MovieInfo.Character("Arthur Fleck / Joker", "Joaquin Phoenix" ),
                        new MovieInfo.Character( "Murray Franklin", "Robert De Niro"),
                        new MovieInfo.Character( "Sophie Dumond", "Zazie Beetz"),
                        new MovieInfo.Character( "Penny Fleck", "Frances Conroy")));
        qcm = new QcmQuestionTmdb(readerFakes, condition, counter);
        when(mockTmdb.getMovie("Joker")).thenReturn(Optional.of(jokerInfo));
    }

    @Test
    void createdQCMWithMainActor(){
        QcmQuestion question = new QcmQuestion(this.id, "Comment s'apelle l'acteur interpretant le role de Arthur Fleck / Joker dans le film Joker ?", "Joaquin Phoenix",
                List.of("Daniel Day-Lewis" +
                        "Sidney Poitier" +
                        "Gregory Peck"));
        Optional<MovieInfo> movieInfo = mockTmdb.getMovie("Joker");
        QcmQuestion open = qcm.goodActorInAMovie(movieInfo.get());

        Assertions.assertEquals("1", question.getText(), open.getText() );
    }

    @Test
    void createdQCMWithYear(){
        QcmQuestion question = new QcmQuestion(this.id, "En quel année le film Joker est-il sortit ?", "2019",
                List.of("2000" +
                        "1987" +
                        "2015"));
        Optional<MovieInfo> movieInfo = mockTmdb.getMovie("Joker");
        QcmQuestion open = qcm.goodYearInAMovie(movieInfo.get());

        Assertions.assertEquals("1", question.getText(), open.getText() );
    }

    @Test
    void createdQCMWithGoodMovie(){
        QcmQuestion question = new QcmQuestion(this.id, "Dans quel film a joué Joaquin Phoenix dans le role de Arthur Fleck / Joker ?", "Joker",
                List.of("Douze hommes en colère" +
                        "Inception" +
                        "Fight Club"));
        Optional<MovieInfo> movieInfo = mockTmdb.getMovie("Joker");
        QcmQuestion open = qcm.goodMovie(movieInfo.get());

        Assertions.assertEquals("1", question.getText(), open.getText() );
    }*/
}
