package root.imdb.factory;

import com.zenika.academy.tmdb.TmdbClient;
import root.beans.Counter;
import root.questionnaire.OpenQuestion;
import com.zenika.academy.tmdb.MovieInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Year;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OpenQuestionTmdbTest {
    TmdbClient       mockTmdb;
    QuestionFactory  qf;
    MovieInfo        jokerInfo;
    OpenQuestionTmdb oq;
    ConditionFactory condition;
    Counter          counter;
    String           id;

/*    @BeforeEach
    void setUp(){
        id = "1";
        qf = new QuestionFactory(mockTmdb);
        condition = new ConditionFactory();
        mockTmdb = mock(TmdbClient.class);
        jokerInfo = new MovieInfo("Joker", Year.of(2019),
                List.of(new MovieInfo.Character("Arthur Fleck / Joker", "Joaquin Phoenix" ),
                        new MovieInfo.Character( "Murray Franklin", "Robert De Niro"),
                        new MovieInfo.Character( "Sophie Dumond", "Zazie Beetz"),
                        new MovieInfo.Character( "Penny Fleck", "Frances Conroy")));
        oq = new OpenQuestionTmdb(condition, counter);
        when(mockTmdb.getMovie("Joker")).thenReturn(Optional.of(jokerInfo));
    }
    @Test
    void verifyTextWithCreatedOpenQuestionWithActorName(){
        OpenQuestion question = new OpenQuestion(this.id, "Comment s'appelle le personnage interpreté par Joaquin Phoenix dans le film Joker ?", "Joaquin Phoenix");
        Optional<MovieInfo> movieInfo = mockTmdb.getMovie("Joker");
        OpenQuestion open = oq.movieInfoToActorName(movieInfo.get());

        Assertions.assertEquals("1", question.getText(), open.getText() );
    }

    @Test
    void verifyTextWithCreatedOpenQuestionWith3Actors(){
        OpenQuestion question = new OpenQuestion(this.id, "Quel film sorti en 2019 contient des personnages joués par les acteurs suivants: Joaquin Phoenix Robert De Niro Zazie Beetz ?", "Joaquin Phoenix");
        Optional<MovieInfo> movieInfo = mockTmdb.getMovie("Joker");
        OpenQuestion open = oq.movieInfoTo3actors(movieInfo.get());

        Assertions.assertEquals("1", question.getText(), open.getText() );
    }

    @Test
    void verifyTextWithCreatedOpenQuestionWithMainCharacter(){
        OpenQuestion question = new OpenQuestion(this.id, "Quel acteur joue le personnage de Arthur Fleck / Joker dans le film Joker ?", "Joaquin Phoenix");
        Optional<MovieInfo> movieInfo = mockTmdb.getMovie("Joker");
        OpenQuestion open = oq.movieInfoToMainCharacter(movieInfo.get());

        Assertions.assertEquals("1", question.getText(), open.getText() );
    }*/
}
