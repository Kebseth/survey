package root.questionnaire;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class OpenQuestionTest {
    public OpenQuestion q;
    String id;

    @BeforeEach
    public void setUp() {
        id = "1";
        q = new OpenQuestion(this.id, "Combien de jours il y a t-il dans une année ?", "365");
    }

    @Test
    void isThereAText() {
        Assertions.assertEquals("Combien de jours il y a t-il dans une année ?", q.getText());
    }

    @Test
    void isTheAnswerCorrect() {
        Assertions.assertTrue(q.correctAnswer("365"));
    }

    @Test
    void isTheAnswerWrong() {
        Assertions.assertFalse(q.correctAnswer("3660"));
    }
}
