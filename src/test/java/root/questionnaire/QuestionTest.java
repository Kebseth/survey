package root.questionnaire;

import org.apache.commons.text.similarity.LevenshteinDistance;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class QuestionTest {
    @Test
    void isLevenshteinWork() {
        LevenshteinDistance levenshtein = new LevenshteinDistance();
        Assertions.assertEquals(5, levenshtein.apply("niche", "chiens"));
    }
}
