package root.questionnaire;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TrueFalseQuestionTest {
    public TrueFalseQuestion qtf;
    String id;

    @BeforeEach

    void setUp() {
        id = "1";
        qtf = new TrueFalseQuestion(this.id, "Ce test est-il fiable ?", true);
    }

    @Test
    void isThereAText() {
        Assertions.assertEquals("Ce test est-il fiable ?", qtf.getText());
    }

    @Test
    void isTheRightAnswerWork() {
        Assertions.assertTrue(qtf.correctAnswer("oui"));
    }

    @Test
    void isTheWrongAnswerFail() {
        Assertions.assertFalse(qtf.correctAnswer("faux"));
    }
}
