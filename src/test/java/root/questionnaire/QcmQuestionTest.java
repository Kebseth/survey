package root.questionnaire;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class QcmQuestionTest {
    public QcmQuestion qcm;
    String id;

    @BeforeEach
    public void setUp(){
        id = "1";
        qcm = new QcmQuestion(this.id, "Quel est la capitale de la France ?", "France", List.of("Londres", "Rennes", "Marseille"));
    }


    @Test
    void isTheTextEquals(){
        Assertions.assertEquals("Quel est la capitale de la France ?", qcm.getText());
    }

    @Test
    void isNumericalAnswerWork(){
        Assertions.assertEquals((qcm.temporaryAnswers.indexOf("France") + 1), 4 );
    }

    @Test
    void isWrongNumericalAnswerFail() {
        Assertions.assertNotEquals(qcm.temporaryAnswers.indexOf("France"), 4);
    }

    @Test
    void isTypoAnswerWork(){
        Assertions.assertTrue(qcm.correctAnswer("France") );
    }

    @Test
    void isLowerTypoAnswerWork(){
        Assertions.assertTrue(qcm.correctAnswer("france") );
    }

    @Test
    void isWrongTyppoFail() {
        Assertions.assertFalse(qcm.correctAnswer("Rennes"));
    }
}
