/*package root.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import root.beans.Counter;
import root.questionnaire.OpenQuestion;
import root.questionnaire.Question;

import java.util.Optional;

public class QuestionRepoTest {

    OpenQuestion openQuestion;
    OpenQuestion secondOpenQuestion;
    OpenQuestion thirdOpenQuestion;
    Counter counter;
    JdbcTemplate jdbc;
    QuestionRepo questionRepo;
    Optional<Question> optional;
    Optional<Question> optional2;
    Optional<Question> optional3;

    @BeforeEach
    void setUp() {
        openQuestion = new OpenQuestion("Quel est la capital de la France ?", "Paris");
        secondOpenQuestion = new OpenQuestion("Quel est la capital de l'Angleterre ?", "Londres");
        thirdOpenQuestion = new OpenQuestion("Quel est la capital de l'Espagne' ?", "Barcelone");
        counter = new Counter();
        jdbc    = new JdbcTemplate();
        questionRepo = new QuestionRepo(counter, jdbc);
        optional = Optional.ofNullable(openQuestion);
        optional2 = Optional.ofNullable(secondOpenQuestion);
        optional3 = Optional.ofNullable(thirdOpenQuestion);
    }

    @Test
    void isTheGetWorking() {
        Assertions.assertEquals(0, questionRepo.getMap().size());
        questionRepo.saveQuestion(optional);
        Assertions.assertEquals(1, questionRepo.getMap().size());
        Assertions.assertEquals("Quel est la capital de la France ?", questionRepo.getQuestion((long) 0).getText());
    }

    @Test
    void isGetAllWorking() {


        questionRepo.saveQuestion(optional);
        questionRepo.saveQuestion(optional2);
        questionRepo.saveQuestion(optional3);
        Assertions.assertEquals(3, questionRepo.getAllQuestion().size());
    }

    @Test
    void isTheIdWorking() {
        questionRepo.saveQuestion(optional);
        questionRepo.saveQuestion(optional2);
        questionRepo.saveQuestion(optional3);
        Assertions.assertEquals(openQuestion, questionRepo.getQuestion((long) 0));
        Assertions.assertEquals(secondOpenQuestion, questionRepo.getQuestion((long) 1));
        Assertions.assertEquals(thirdOpenQuestion, questionRepo.getQuestion((long) 2));
    }
}*/
