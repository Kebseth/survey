drop table if exists question;
drop table if exists suggestion;
create table question
(
    id         text primary key,
    question_text  text not null,
    correct_answer text,
    correct_answer_bool   boolean
);
create table suggestion
(
    question_id text references question(id),
    choice   text not null,
      PRIMARY KEY (question_id, choice)
);
BEGIN TRANSACTION;
insert into public.question
values('1', 'Quel film est sorti dans l''année 2016 qui a comme cast: Will Smith, Margot Robbie, Joel Kinnaman ?','Suicide Squad', null);
insert into public.question
values('2', 'Dans le film Burnt, Sienna Miller joue le personnage de:', 'Helene', null);
insert into public.question
values('3', 'Le personnage Doris Miller dans le film Pearl Harbor est joué par l''acteur/l''actrice Cuba Gooding Jr."', null, true);
insert into public.question
values('4', 'Quel film est sorti dans l''année 2007 qui a comme cast: Catherine Zeta-Jones, Aaron Eckhart, Abigail Breslin ?','No Reservations', null);
insert into public.question
values('5', 'Dans le film Titanic, Leonardo DiCaprio joue le personnage de:', 'Jack Dawson', null);
insert into public.question
values('6', 'Le personnage Dr. John Cawley dans le film Shutter Island est joué par l''acteur/l''actrice Emily Mortimer', null, false);
COMMIT;
BEGIN TRANSACTION;
insert into public.suggestion
values('2', 'Sara');
insert into public.suggestion
values('2', 'Helena');
insert into public.suggestion
values('2', 'Gabriela');
insert into public.suggestion
values('5', 'Ruth Dewitt Bukater');
insert into public.suggestion
values('5', 'Rose DeWitt Bukater');
insert into public.suggestion
values('5', 'Carl Hewl');
COMMIT;