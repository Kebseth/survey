package root.representation;

public enum QuestionType {
    OPEN,
    TRUE_FALSE,
    QCM
}
