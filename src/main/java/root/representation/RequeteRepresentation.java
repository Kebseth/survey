package root.representation;

public class RequeteRepresentation {
    private String requete;

    public String getRequete() { return requete; }

    public RequeteRepresentation(String requete) {
        this.requete = requete;
    }
}
