package root.representation;

import java.util.ArrayList;
import java.util.List;

public class QuestionRepresentation {

    private QuestionType type;
    private String   id;
    private String text;
    private List<String> choices;

    public QuestionRepresentation(String text, String id, QuestionType type) {
        this.text = text;
        this.id   = id;
        this.type = type;
    }

    public QuestionRepresentation(String text, String id, QuestionType type, List<String> choices) {
        this.choices = choices;
        this.text    = text;
        this.id      = id;
        this.type    = type;
    }

    public QuestionType      getType()    { return this.type;}
    public String              getId()      { return this.id;}
    public String            getText()    { return this.text; }
    public List<String> getChoices() { return this.choices; }
}
