package root.representation;

import root.imdb.Difficulties;

public class DifficultiesRepresentation {
    public Difficulties difficulty;

    DifficultiesRepresentation(Difficulties difficulty) {
        this.difficulty = difficulty;
    }
}
