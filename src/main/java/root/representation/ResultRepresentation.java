package root.representation;

public class ResultRepresentation {
    private String result;

    public ResultRepresentation(String result) {
        this.result = result;
    }

    public String getResult() { return this.result; }
}
