package root.representation;

public class ErrorRepresentation {
    private String error;

    public ErrorRepresentation(String error) {
        this.error= error;
    }

    public String getError() { return this.error; }
}
