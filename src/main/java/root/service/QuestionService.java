package root.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import root.imdb.Difficulties;
import root.imdb.factory.QuestionFactory;
import root.questionnaire.QcmQuestion;
import root.questionnaire.Question;
import root.representation.*;
import root.repository.QuestionRepo;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class QuestionService {

    private QuestionFactory factory;
    private QuestionRepo    repo;

    @Autowired
    public QuestionService(QuestionRepo repo, QuestionFactory factory) {
        this.repo    = repo;
        this.factory = factory;
    }

    public QuestionService service() {
        return new QuestionService(this.repo, this.factory);
    }

    public List<QuestionRepresentation> getAllRepresentationQuestion(){
        ArrayList<QuestionRepresentation> arrayOfReprensentationQuestion = new ArrayList<>();

        Collection <Question> all = this.repo.getAllQuestion();

        // Transformation des Questions en RepresentationQuestions
        List<QuestionRepresentation> qcmList = all.stream()
                                                    .filter(q -> q instanceof QcmQuestion)
                                                    .map(q -> new QuestionRepresentation(q.getText(), q.getId(), q.getType(), ((QcmQuestion) q).temporaryAnswers))
                                                    .collect(Collectors.toList());

        List<QuestionRepresentation> otherList = all.stream()
                                                    .filter(q -> !(q instanceof QcmQuestion))
                                                    .map(q -> new QuestionRepresentation(q.getText(), q.getId(), q.getType()))
                                                    .collect(Collectors.toList());
        otherList.addAll(qcmList);
        List<QuestionRepresentation> fuseList = new ArrayList<>(otherList);
        // .sorted(Comparator.comparingInt(q -> (int) q.getId()))

        return fuseList;
        /*for (Question q:all) {
            if(q.getClass() == QcmQuestion.class) {
                arrayOfReprensentationQuestion.add(new QuestionRepresentation(
                        q.getText(),
                        q.getQuestionId(),
                        ((QcmQuestion) q).temporaryAnswers));
            } else {
                arrayOfReprensentationQuestion.add(new QuestionRepresentation(
                        q.getText(),
                        q.getQuestionId()));
            }
        }*/
    }

    public Object representationQuestionId(String id) {
        Optional<Question> q = this.repo.getQuestion(id);
        if(q.isPresent()) {
            if(q.get().getClass() == QcmQuestion.class) {
                return new QuestionRepresentation(q.get().getText(), q.get().getId(), q.get().getType(), ((QcmQuestion) q.get()).temporaryAnswers);
            } else {
                return new QuestionRepresentation(q.get().getText(), q.get().getId(), q.get().getType());
            }
        } else return new ErrorRepresentation("La question n'éxiste pas.");
    }
    // random difficulty
    public Object postQuestion(String movieName){
        Optional<? extends Question> q = factory.getDifficulty(movieName, randomizeDifficulties());
        if(q.isPresent()) {
            repo.saveQuestion((Optional<Question>) q);

            if(q.get().getClass() == QcmQuestion.class) {
                return new QuestionRepresentation(q.get().getText(), q.get().getId(), q.get().getType(), ((QcmQuestion) q.get()).temporaryAnswers);
            } else {
                return new QuestionRepresentation(q.get().getText(), q.get().getId(), q.get().getType());
            }
        } else return new ErrorRepresentation("Votre film ne semble pas exister !");
    }
    // choose difficulty
    public Object postQuestion(String movieName, DifficultiesRepresentation difficulty){
        Difficulties dif = difficulty.difficulty;
        Optional<? extends Question> q = factory.getDifficulty(movieName, dif);
        if(q.isPresent()) {
            repo.saveQuestion((Optional<Question>) q);

            if(q.get().getClass() == QcmQuestion.class) {
                return new QuestionRepresentation(q.get().getText(), q.get().getId(), q.get().getType(), ((QcmQuestion) q.get()).temporaryAnswers);
            } else {
                return new QuestionRepresentation(q.get().getText(), q.get().getId(), q.get().getType());
            }
        } else return new ErrorRepresentation("Votre film ne semble pas exister !");
    }

    public void deleteQuestion(String id) {
        //new RequeteRepresentation(
        repo.deleteQuestion(id);
    }

    public Object tryAnswer(String id, String answer) {
        Optional<Question> question = this.repo.getQuestion(id);
        if(question.isPresent()) {
            return new ResultRepresentation(question.get().correctAnswer(answer).toString());
        } else return new ErrorRepresentation("Votre film ne semble pas exister !");
    }

    private Difficulties randomizeDifficulties(){
        int rand = new Random().nextInt(3);
        switch(rand) {
            case 1: return Difficulties.MEDIUM;
            case 2: return Difficulties.HARD;
            default: return Difficulties.EASY;
        }
    }
}
