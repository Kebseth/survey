package root.scrapping;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import root.beans.WriteTxt;

import java.io.IOException;
import java.util.ArrayList;

public class ScrapListOfActors {
    public static void main(String args[]){

        Document document;
        try {
            //Get Document object after parsing the html from given url.
                document = Jsoup.connect("https://www.imdb.com/list/ls058011111/?sort=list_order,asc&mode=detail&page=1").get();
                ArrayList listOfActors = new ArrayList();

                Elements elements = document.select("h3").select("a");
                elements.forEach(e -> {
                    listOfActors.add(e.text());
                });

                // writting
                WriteTxt wa = new WriteTxt();
                wa.bufferedWriter(listOfActors, WriteTxt.FILENAMEACTORS);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
