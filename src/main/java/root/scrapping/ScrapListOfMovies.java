package root.scrapping;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import root.beans.WriteTxt;

import java.io.IOException;
import java.util.ArrayList;

public class ScrapListOfMovies {

    public static void main(String args[]){

        Document document;
        try {
            //Get Document object after parsing the html from given url.
            document = Jsoup.connect("https://www.imdb.com/search/title/?groups=top_250&sort=user_rating").get();
            ArrayList listOfActors = new ArrayList();

            Elements elements = document.select("h3").select("a");
            elements.forEach(e -> {
                listOfActors.add(e.text());
            });

            // writting
            WriteTxt wa = new WriteTxt();
            wa.bufferedWriter(listOfActors, WriteTxt.FILENAMEMOVIES);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
