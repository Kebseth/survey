package root.questionnaire;

import java.util.List;
import java.util.Optional;

public class Quizz {
    private long id;
    private long user_id;
    private long score_id;

    public long getId() { return id; }

    public long getScore_id() { return score_id; }

    public long getUser_id() { return user_id; }

}
