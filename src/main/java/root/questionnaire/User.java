package root.questionnaire;

import java.util.Scanner;

class User {
    private Integer score;

    public Integer getScore() {
        return score;
    }

    public Integer setScore(Integer score) {
        return this.score = score;
    }
    public static String getName(Scanner sc) {
        System.out.println("Quel est votre nom ?");
        String userName = sc.nextLine();
        return userName;
    }

    public User(String username) {
        score = 0;
    }

    public Integer addAPoint(String userAnswer, Question question) {
        Integer score = this.getScore();
        if(question.correctAnswer( userAnswer)) {
            return this.setScore(score + question.getPoint());
        } else {
            return score;
        }
    }

}