package root.questionnaire;

import root.representation.QuestionType;

public class TrueFalseQuestion extends Question {

    private String       text;
    private Boolean      answer;
    private int          point;
    private QuestionType type;
    private String       id;


    public  String      getText()    { return this.text; }
    public  boolean     getAnswer()  { return answer; }
    public  int         getPoint()   { return this.point; }
    public String       getId()      { return id; }

    public QuestionType getType()    { return this.type; }

    public TrueFalseQuestion(String id, String questionParameter, Boolean answerParameter) {
        this.text   = questionParameter;
        this.answer = answerParameter;
        this.point  = 1;
        this.id     = id;
        this.type   = QuestionType.TRUE_FALSE;
    }

    public Boolean correctAnswer(String userAnswer) {
        String lowerUserAnswer = userAnswer.toLowerCase();
        if(!answer) {
            return isAFault(lowerUserAnswer);
        } else {
            return isATruth(lowerUserAnswer);
        }
    }

    private boolean isAFault(String lowerUserAnswer) {
        return lowerUserAnswer.equals("faux") || lowerUserAnswer.equals("false") || lowerUserAnswer.equals("non") || lowerUserAnswer.equals("2");
    }

    private boolean isATruth(String lowerUserAnswer) {
        return lowerUserAnswer.equals("vrai") || lowerUserAnswer.equals("true") || lowerUserAnswer.equals("oui") || lowerUserAnswer.equals("1");
    }
}
