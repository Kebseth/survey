package root.questionnaire;

import org.springframework.stereotype.Component;
import root.representation.QuestionType;

import java.util.concurrent.atomic.AtomicLong;

@Component
public abstract class Question {

    private String id;
    public abstract String getText();
    abstract int    getPoint();

    public Question () {
    }

    public String     getId()     { return this.id;}

    public void setQuestionId(String id) { this.id = id; }

    public abstract Boolean correctAnswer(String userAnswer);

    public abstract QuestionType getType();
}



