package root.questionnaire;

import org.apache.commons.text.similarity.LevenshteinDistance;
import root.representation.QuestionType;

public class OpenQuestion extends Question {
    private String       text;
    private String       answer;
    private int          point;
    private QuestionType type;
    private String       id;

    public String       getText()   { return this.text; }
    public String       getAnswer() { return this.answer; }
    public int          getPoint()  { return this.point; }
    public QuestionType getType()   { return this.type; }
    public String       getId()     { return this.id; }

    public OpenQuestion(String id, String questionParameter, String answerParameter){
        this.text   = questionParameter;
        this.answer = answerParameter;
        this.point  = 3;
        this.type   = QuestionType.OPEN;
        this.id     = id;
    }

    public Boolean correctAnswer(String userAnswer) {
        LevenshteinDistance levenshtein = new LevenshteinDistance();
        String user = userAnswer.toLowerCase();
        String expected = this.getAnswer().toLowerCase();
        return levenshtein.apply(user, expected) < 2;
    }
}
