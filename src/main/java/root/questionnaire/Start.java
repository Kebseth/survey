package root.questionnaire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.GsonBuilderUtils;
import org.w3c.dom.ls.LSOutput;
import root.beans.Counter;
import root.imdb.Difficulties;
import root.imdb.ReaderFakes;
import root.imdb.factory.QuestionFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import root.repository.QuestionRepo;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;


@Component
class Start implements CommandLineRunner {

    QuestionFactory questionFactory;
    Scanner         scanner;
    ReaderFakes     readerFakes;
    QuestionRepo    repo;
    Counter         counter;

    @Autowired
    public Start(QuestionFactory questionFactory, Scanner scanner, ReaderFakes readerFakes, QuestionRepo repo, Counter counter) {
        this.questionFactory = questionFactory;
        this.scanner         = scanner;
        this.readerFakes     = readerFakes;
        this.repo            = repo;
        this.counter         = counter;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Avant repo");


        //OpenQuestion question = new OpenQuestion("Quel est le cheval d'Henry", "Kiki");
        //TrueFalseQuestion question = new TrueFalseQuestion("soleil jaune ?", true);

        //QcmQuestion question = new QcmQuestion(counter.getRandomID(), "mon nom ?", "Richard", List.of("r", "v", "h"));
        //Optional<Question> q =  Optional.ofNullable(question);
        //repo.saveQuestion(q);
        //repo.deleteQuestion(question.getQuestionId());

        // Liste des instances Question
        //ArrayList<Optional<Question>> questions = this.createQuestions();

        /*for (Optional<Question> q: questions) {
            if(q.isPresent()) {
                System.out.println(q.get().getText());
                // Vérification si la question une une QCM
                if(q.get() instanceof QcmQuestion){
                    ((QcmQuestion) q.get()).randomizeAnswers();
                    System.out.println(q.get().getAnswer());
                }

                // Récuperation de la réponse utilisateur
                String userAnswer = scanner.nextLine().toLowerCase();
                player1.addAPoint(userAnswer, q.get());

            }
        }*/

        //System.out.println(getResult(player1.getScore(), name,questions ));
    }

    public ArrayList<Optional<Question>> createQuestions() throws IOException {
        ArrayList<Optional<Question>> questionsList = new ArrayList<>();

        for(int i=0; i < 10; ++i){

            List<String> movie = readerFakes.listOfFake(new File(ReaderFakes.FILENAMEMOVIES), 1);

            Optional<Question> q = (Optional<Question>) questionFactory.getDifficulty(movie.get(0), randomizeDifficulties());
            repo.saveQuestion(q);
            questionsList.add(q);
        }
        return questionsList;
    }

    private Difficulties randomizeDifficulties(){
        int rand = new Random().nextInt(3);
        switch(rand) {
            case 1: return Difficulties.MEDIUM;
            case 2: return Difficulties.HARD;
            default: return Difficulties.EASY;
        }
    }

    private String getResult(Integer score, String name, ArrayList<Optional<Question>> questionList) {

        if (score < 5) {
            return "Ce n'est pas fameux, " + name + ", votre score est de " + score + "/" + this.getTotal(questionList) + "..." ;
        } else {
            return "Bravo, "+ name + "! Votre score est de " + score + "/" + this.getTotal(questionList) + ".";
        }
    }

    private int getTotal(ArrayList<Optional<Question>> questionList){
        int total = 0;
        for (Optional<Question> q:questionList) {
            if(q.isPresent()) {
                total += q.get().getPoint();
            }
        }
        return total;
    }
}
