package root.questionnaire;

import org.apache.commons.text.similarity.LevenshteinDistance;
import root.representation.QuestionType;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QcmQuestion extends Question {

    private int           point;
    private String        id;
    private String        text;
    private String        answer;
    private List<String>  choices;
    private QuestionType  type;
    public  ArrayList<String>  randomizeAnswers;
    public  ArrayList<String> temporaryAnswers;

    public int               getPoint()      { return this.point; }
    public String            getText()       { return this.text; }
    public String            getGoodAnswer() { return this.answer; }
    public QuestionType      getType()       { return this.type; }
    public List<String>      getChoices()    { return this.choices; }
    public String            getId()         { return this.id;}


    public QcmQuestion(String id,  String questionParameter, String answerParameter, List<String> choices) {
        this.text    = questionParameter;
        this.choices = choices;
        this.answer  = answerParameter;
        this.point   = 2;
        this.id      = id;
        this.type    = QuestionType.QCM;
        temporaryAnswers();
    }

    private void temporaryAnswers() {
        temporaryAnswers = new ArrayList<>();
        temporaryAnswers.addAll(this.choices);
        temporaryAnswers.add(this.answer);
    }

    public void randomizeAnswers() {
        Collections.shuffle(temporaryAnswers);
        randomizeAnswers = temporaryAnswers;
    }

    public ArrayList<String> getAnswer() {
        ArrayList<String> display = new ArrayList<>();
        for(int i=0; i<4; ++i) {
            display.add(this.temporaryAnswers.get(i));
        }
        return display;
    }

    public Boolean correctAnswer(String userAnswer) {
        // Chercher l'index
        int goodIndex = this.temporaryAnswers.indexOf(answer);
        String convertAnswer = String.valueOf(goodIndex + 1);

        if(userAnswer.equals(convertAnswer)) {
            return true;
        } else {
            LevenshteinDistance levenshtein = new LevenshteinDistance();
            String user = userAnswer.toLowerCase();
            String expected = this.getGoodAnswer().toLowerCase();
            return levenshtein.apply(user, expected) < 2;
        }
    }
}
