package root.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Scanner;

@Configuration
public class MyScanner {

    @Bean
    public Scanner myScannering() {
        return new Scanner(System.in);
    }
}
