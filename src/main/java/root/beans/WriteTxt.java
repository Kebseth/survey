package root.beans;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class WriteTxt {
    public static final String FILENAMEACTORS = new File("src/main/java/root/scrapping/actors.txt").getAbsolutePath();
    public static final String FILENAMEMOVIES = new File("src/main/java/root/scrapping/movies.txt").getAbsolutePath();
    public static final String FILENAMEJSON   = new File("src/main/java/root/repository/listOfQuestions.txt").getAbsolutePath();

    public static void bufferedWriter(ArrayList data, String file) {
        FileWriter fr = null;
        BufferedWriter br = null;
        try{
            fr = new FileWriter(file);
            br = new BufferedWriter(fr);
            for (Object actor:data) {
                String sentence = actor + System.lineSeparator();
                br.write(sentence);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                br.close();
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}