package root.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class Counter {
    private String counter;

    @Autowired
    public Counter() {
    }

    public String getRandomID() { return UUID.randomUUID().toString(); }

    public Counter myCounter() {
        return new Counter();
    }
}
