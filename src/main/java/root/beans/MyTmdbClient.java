package root.beans;

import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;

@Configuration
public class MyTmdbClient{

    @Bean
    public File apiKeyFile() {
        return new File(new File("src/main/resources/ApiRessources.txt").getAbsolutePath());
    }

    @Bean
    public TmdbClient myTmdb(File apiKeyFile) throws IOException {
        return new TmdbClient(apiKeyFile);
    }

}
