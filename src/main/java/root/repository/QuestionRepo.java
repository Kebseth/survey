package root.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import root.beans.Counter;
import root.questionnaire.OpenQuestion;
import root.questionnaire.QcmQuestion;
import root.questionnaire.Question;
import root.questionnaire.TrueFalseQuestion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Component
public class QuestionRepo {

    private Counter             counter;
    private Map<String, Question> questionMap;
    JdbcTemplate template;

    @Autowired
    public QuestionRepo(Counter counter, JdbcTemplate template) {
        this.questionMap = new HashMap<>();
        this.counter     = counter;
        this.template    = template;
    }

    public Map<String, Question> getMap()     { return this.questionMap; }
    public Counter               getCounter() { return this.counter; }

    public Optional<Question>    getQuestion(String id) {
        String sqlRequest = "SELECT * FROM question q LEFT JOIN suggestion s ON q.id = s.question_id WHERE id = ?";
        ResultSetExtractor<Question> resultSetExtractor = (rs) -> {
            if(!rs.next()) {
                return  null;
            }
            String questionText = rs.getString("question_text");
            String correctAnswer = rs.getString("correct_answer");
            boolean correctAnswerBool = rs.getBoolean("correct_answer_bool");
            String suggestion = rs.getString("choice");

            if (suggestion == null) {
                if ((rs.getString("correct_answer") != null)) {
                    return new OpenQuestion(id, questionText, correctAnswer);
                } else {
                    return new TrueFalseQuestion(id, questionText, correctAnswerBool);
                }
            } else {
                List<String> suggestions = new ArrayList<>();
                suggestions.add(suggestion);
                while (rs.next()){
                    suggestions.add(rs.getString("choice"));
                }
                return new QcmQuestion(id, questionText, correctAnswer, suggestions);
            }
        };
        return Optional.ofNullable(template.query(sqlRequest, resultSetExtractor, id));
    }

    public void deleteQuestion(String id) {
        String sqlQuestion = "DELETE FROM question " +
                             "WHERE question.id = ?";
        String sqlSuggestion = "DELETE FROM suggestion " +
                               "WHERE suggestion.question_id = ?";
        template.update(sqlSuggestion, id);
        template.update(sqlQuestion, id);
    }

    public boolean saveQuestion(Optional<Question> question) {

        if(question.isPresent()) {
            // question.get().setQuestionId(getCounter().getID());
            if(question.get() instanceof OpenQuestion) {
                saveOpenQuestion((OpenQuestion) question.get());
            } else if (question.get() instanceof TrueFalseQuestion) {
                saveTrueFalseQuestion((TrueFalseQuestion) question.get());
            } else if (question.get() instanceof QcmQuestion) {
                saveQCMQuestion((QcmQuestion) question.get());
            }
            return true;
        } else return false;
    }

    public List<Question> getAllQuestion() {
        String sqlRequest = "SELECT * FROM question q LEFT JOIN suggestion s ON q.id = s.question_id";
        ResultSetExtractor<List<Question>> resultSetExtractor = resultSet ->{
            List<Question> questionList = new ArrayList<>();
            if(!resultSet.next())
                return questionList;
            while(!resultSet.isAfterLast()){
                String id = resultSet.getString("id");
                String questionText = resultSet.getString("question_text");
                String correctAnswer = resultSet.getString("correct_answer");
                boolean correctAnswerB = resultSet.getBoolean("correct_answer_bool");
                String suggestion = resultSet.getString("choice");
                if(suggestion == null){
                    if(correctAnswer != null){
                        questionList.add(new OpenQuestion(id,questionText,correctAnswer));
                        resultSet.next();
                    }
                    else{
                        questionList.add(new TrueFalseQuestion(id,questionText,correctAnswerB));
                        resultSet.next();
                    }
                }
                else{
                    List<String> suggestions = new ArrayList<>();
                    suggestions.add(suggestion);
                    while (resultSet.next() && resultSet.getString("id").equals(id)){
                        suggestions.add(resultSet.getString("choice"));
                    }
                    questionList.add(new QcmQuestion(id,questionText,correctAnswer, suggestions));
                }
            }
            return questionList;
        };
        return template.query(sqlRequest,resultSetExtractor);
    }

    private void saveOpenQuestion(OpenQuestion question) {
        String text = question.getText();
        String answer = question.getAnswer();
        template.update("INSERT INTO question(id, question_text, correct_answer) " +
                "VALUES (?, ?, ?)", question.getId(), text, answer);
    }

    private void saveTrueFalseQuestion(TrueFalseQuestion question) {
        String text = question.getText();
        boolean answer = question.getAnswer();
        template.update("INSERT INTO question(id, question_text, correct_answer_bool) " +
                "VALUES (?, ?, ?)", question.getId(), text, answer);
    }

    private void saveQCMQuestion(QcmQuestion question) {
        String text = question.getText();
        String answer = question.getGoodAnswer();
        List<String> choices = question.getChoices();
        String sqlQuestion = "INSERT INTO question(id, question_text, correct_answer) " +
                "VALUES (?, ?, ?); ";
        String sqlChoice = "INSERT INTO suggestion(question_id, choice) " +
                "VALUES (?, ?); ";


        template.update(sqlQuestion, question.getId(), text, answer);
        template.update(sqlChoice, question.getId(), choices.get(0));
        template.update(sqlChoice, question.getId(), choices.get(1));
        template.update(sqlChoice, question.getId(), choices.get(2));
    }
}
