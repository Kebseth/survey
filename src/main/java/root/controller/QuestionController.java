package root.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import root.representation.DifficultiesRepresentation;
import root.representation.QuestionRepresentation;
import root.service.QuestionService;

import java.util.List;

@RestController
@RequestMapping("/questions")
public class QuestionController {

    private QuestionService service;

    @Autowired
    public QuestionController(QuestionService service) {
        this.service = service;
    }

    @GetMapping("")
    public List<QuestionRepresentation> questions() {
        return this.service.getAllRepresentationQuestion();
    }

    @PostMapping("/{movieName}")
    public Object createQuestion(@PathVariable() String movieName, @RequestBody(required = false) DifficultiesRepresentation dif) {
        return service.postQuestion(movieName, dif);
    }

    @GetMapping("/{id}")
    public void questionId(@PathVariable String id) {
        service.representationQuestionId(id);
    }

    @PostMapping("{id}/{answer}")
    public Object tryAnswer(@PathVariable String id, @PathVariable String answer) {
        return service.tryAnswer(id, answer);
    }

    @DeleteMapping("{id}")
    public void  deleteQuestion(@PathVariable String id) {
        service.deleteQuestion(id);
    }
}
