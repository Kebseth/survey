package root.imdb;

import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class ReaderFakes {

    // Variable pour lire la data
    public static final String FILENAMEACTORS = new File("src/main/java/root/scrapping/actors.txt").getAbsolutePath();
    public static final String FILENAMEMOVIES = new File("src/main/java/root/scrapping/movies.txt").getAbsolutePath();

    // Lecture des fichiers .txt
    public List<String> listOfFake(File filename, int longueur) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        List<String> randomList = new ArrayList<String>();
        File file = new File(String.valueOf(filename));
        FileInputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);

        String line;

        while((line = br.readLine()) != null){
            list.add(line);
        }

        for (int i = 0; i<longueur; ++i) {
            int rand = new Random().nextInt(list.size());
            randomList.add(list.get(rand).toString());
        }
        br.close();
        return randomList;
    }
}
