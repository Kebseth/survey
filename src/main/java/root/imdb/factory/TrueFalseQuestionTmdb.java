package root.imdb.factory;

import com.zenika.academy.tmdb.MovieInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import root.beans.Counter;
import root.imdb.ReaderFakes;
import root.questionnaire.TrueFalseQuestion;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class TrueFalseQuestionTmdb {

    private ReaderFakes      readerFakes;
    private ConditionFactory condition;
    Counter          counter;

    @Autowired
    public TrueFalseQuestionTmdb(ReaderFakes readerFakes, ConditionFactory condition, Counter counter) {
        this.condition   = condition;
        this.readerFakes = readerFakes;
        this.counter     = counter;
    }

    //public ConditionFactory getCondition() { return this.condition; }

    // Question 1
    public TrueFalseQuestion goodYear(MovieInfo info){
        String question = "Le film " + info.title + " est sortit en " + info.year + " ?";
        return new TrueFalseQuestion(counter.getRandomID(), question, true);
    }

    // Question 2
    public TrueFalseQuestion wrongYear(MovieInfo info){
        int wrongYear   = Integer.parseInt(String.valueOf(info.year)) - 2;
        String question = "Le film " + info.title + " est sortit en " + wrongYear + " ?";
        return new TrueFalseQuestion(counter.getRandomID(), question, false);
    }

    // Question 3
    public TrueFalseQuestion goodActor(MovieInfo info){
        if(condition.isThereEnoughActor(info, 0)) {
            String question = info.cast.get(0).actorName + " joue dans le film " + info.title + " ?";
            return new TrueFalseQuestion(counter.getRandomID(), question, true);
        } else return null;
    }

    // Question 4
    public TrueFalseQuestion wrongActor(MovieInfo info){
        List<String> fakeAnswer = new ArrayList<>();
        try {
            fakeAnswer = readerFakes.listOfFake(new File(ReaderFakes.FILENAMEACTORS), 3);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String question = fakeAnswer.get(0) + " joue dans le film " + info.title + " ?";
        return new TrueFalseQuestion(counter.getRandomID(), question, false);
    }

    // Question 5
    public TrueFalseQuestion goodMovie(MovieInfo info){
        if(condition.isThereEnoughCharacter(info, 0)) {
            String question =  info.cast.get(0).characterName + " est un personnage du film " + info.title + " ?";
            return new TrueFalseQuestion(counter.getRandomID(), question, true);
        } else return null;
    }

    // Question 6
    public TrueFalseQuestion wrongMovie(MovieInfo info){
        List<String> fakeAnswer = new ArrayList<>();
        if(condition.isThereEnoughCharacter(info, 0)) {
            try {
                fakeAnswer = readerFakes.listOfFake(new File(ReaderFakes.FILENAMEMOVIES), 3);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String question =  info.cast.get(0).characterName + "  est un personnage du film " + fakeAnswer.get(0) + " ?";
            return new TrueFalseQuestion(counter.getRandomID(), question, false);
        } else return null;
    }
}
