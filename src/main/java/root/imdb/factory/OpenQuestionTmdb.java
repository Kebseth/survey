package root.imdb.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import root.beans.Counter;
import root.questionnaire.OpenQuestion;
import com.zenika.academy.tmdb.MovieInfo;

import java.time.Year;
import java.util.Optional;

@Component
public class OpenQuestionTmdb {

    private ConditionFactory condition;
    private Counter          counter;

    @Autowired
    public OpenQuestionTmdb(ConditionFactory condition, Counter counter) {
        this.counter   = counter;
        this.condition = condition;
    }

    // Question 1
    public OpenQuestion movieInfoToMainCharacter(MovieInfo infos) {
        if(condition.isThereEnoughActor(infos, 0) && condition.isThereEnoughCharacter(infos, 0)) {
            String question = "Quel acteur joue le personnage de " + infos.cast.get(0).characterName + " dans le film " + infos.title + " ?";
            String answer   = infos.cast.get(0).actorName;
            return new OpenQuestion(counter.getRandomID(), question, answer);
        } else return null;
    }

    // Question 2
    public OpenQuestion movieInfoTo3actors(MovieInfo infos) {
        if(condition.isThereEnoughActor(infos, 2)) {
            String question = "Quel film sorti en " + infos.year + " contient des personnages joués par les acteurs suivants: " +
                    infos.cast.get(0).actorName + " " + infos.cast.get(1).actorName + " " + infos.cast.get(2).actorName + " ?";
            String answer = infos.title;
            return new OpenQuestion(counter.getRandomID(), question, answer);
        } else return null;
    }

    // Question 3
    public OpenQuestion movieInfoToActorName(MovieInfo infos) {
        if(condition.isThereEnoughActor(infos, 0) && condition.isThereEnoughCharacter(infos, 0)) {
            String question = "Comment s'appelle le personnage interpreté par " + infos.cast.get(0).actorName+ " dans le film " + infos.title + " ?";
            String answer   =  infos.cast.get(0).characterName;
            return new OpenQuestion(counter.getRandomID(), question, answer);
        } else return null;
    }

    public OpenQuestion goodYear(MovieInfo infos) {
        String question = "En quel année est sortit le film " + infos.title + " ?";
        Year   answer   = infos.year;
        return new OpenQuestion(counter.getRandomID(), question, answer.toString());
    }
}
