package root.imdb.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import root.beans.Counter;
import root.imdb.ReaderFakes;
import root.questionnaire.QcmQuestion;
import com.zenika.academy.tmdb.MovieInfo;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class QcmQuestionTmdb {

    private ReaderFakes      readerFakes;
    private ConditionFactory condition;
    Counter          counter;

    @Autowired
    public QcmQuestionTmdb(ReaderFakes readerFakes, ConditionFactory condition, Counter counter) {
        this.condition   = condition;
        this.readerFakes = readerFakes;
        this.counter = counter;
    }

    //public ConditionFactory getCondition() { return this.condition; }

    // Question 1
    public QcmQuestion goodActorInAMovie(MovieInfo info) {
        if(condition.isThereEnoughActor(info, 0) && condition.isThereEnoughCharacter(info, 0)) {
            String question = "Comment s'apelle l'acteur interpretant le role de " + info.cast.get(0).characterName + " dans le film " + info.title + " ?";
            String answer   = info.cast.get(0).actorName;
            List<String> fakeAnswers = null;
            try {
                fakeAnswers = readerFakes.listOfFake(new File(ReaderFakes.FILENAMEACTORS), 3);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return new QcmQuestion(counter.getRandomID(), question, answer, List.of( fakeAnswers.get(0), fakeAnswers.get(1),  fakeAnswers.get(2)));
        } else return null;
    }

    // Question 2
    public QcmQuestion goodMovie(MovieInfo info) {
        if(condition.isThereEnoughActor(info, 0) && condition.isThereEnoughCharacter(info, 0)) {
            String question = "Dans quel film a joué " + info.cast.get(0).actorName + " dans le role de " + info.cast.get(0).characterName + " ?";
            String answer   = info.title;
            List<String> fakeAnswers = null;
            try {
                fakeAnswers =  readerFakes.listOfFake(new File(ReaderFakes.FILENAMEMOVIES), 3);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return new QcmQuestion(counter.getRandomID(), question, answer, List.of(fakeAnswers.get(0),  fakeAnswers.get(1), fakeAnswers.get(2)));
        } else return null;
    }

    // Question 3
    public QcmQuestion goodYearInAMovie(MovieInfo info) {
        String question = "En quel année le film " + info.title + " est-il sortit ?";
        String answer   = String.valueOf(info.year);
        List<String> fakeAnswers = new ArrayList();
        int i = 0;
        while (i < 3){
            String rand = String.valueOf(Math.round(1970 + (Math.random() * (2020 - 1970))));
            if(!fakeAnswers.contains(rand) && rand != answer){
                fakeAnswers.add(rand);
                ++i;
            }
        }
        return new QcmQuestion(counter.getRandomID(), question, answer, List.of(fakeAnswers.get(0), fakeAnswers.get(1), fakeAnswers.get(2)));
    }
}
