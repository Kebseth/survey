package root.imdb.factory;

import com.zenika.academy.tmdb.MovieInfo;
import org.springframework.stereotype.Component;

@Component
public class ConditionFactory {
    public Boolean isThereEnoughActor(MovieInfo info, int actors) {
        return !info.cast.get(actors).actorName.isEmpty();
    }

    public Boolean isThereEnoughCharacter(MovieInfo info, int character) {
        return !info.cast.get(character).characterName.isEmpty();
    }


}
