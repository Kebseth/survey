package root.imdb.factory;

import com.zenika.academy.tmdb.TmdbClient;
import root.imdb.Difficulties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import root.questionnaire.OpenQuestion;
import root.questionnaire.QcmQuestion;
import root.questionnaire.Question;
import com.zenika.academy.tmdb.MovieInfo;
import root.questionnaire.TrueFalseQuestion;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Optional;
import java.util.Random;

import static java.lang.StrictMath.ceil;

@Component
public class QuestionFactory {

    private TrueFalseQuestionTmdb tfQuestion;
    private TmdbClient            myTmdb;
    private OpenQuestionTmdb      openQuestion;
    private QcmQuestionTmdb       qcmQuestion;
    public  int                   rand;

    @Autowired
    public QuestionFactory(OpenQuestionTmdb openQuestionTmdb, QcmQuestionTmdb qcmQuestionTmdb, TrueFalseQuestionTmdb tfQuestion, TmdbClient myTmdb) throws IOException {
        this.myTmdb       = myTmdb;
        this.openQuestion = openQuestionTmdb;
        this.qcmQuestion  = qcmQuestionTmdb;
        this.tfQuestion   = tfQuestion;
    }

    // Constructeur pour test
    public QuestionFactory(TmdbClient client) {
        myTmdb = client;
    }

    public TrueFalseQuestionTmdb getTFQuestion()            { return tfQuestion; }
    public Optional<MovieInfo>   getMovieInfo(String movie) { return myTmdb.getMovie(movie); }
    public OpenQuestionTmdb      getOpenQuestion()          { return openQuestion; }
    public QcmQuestionTmdb       getQcmQuestion()           { return qcmQuestion; }

    // Créer une question en fonction de la difficulté
    public Optional<? extends Question> getDifficulty(String inputMovie, Difficulties dif) {
        switch(dif){
            case EASY  : return createdTrueFalseQuestionImdb(inputMovie);
            case MEDIUM: return createdQcmQuestionImdb(inputMovie);
            case HARD  : return createdOpenQuestionImdb(inputMovie);
            default    : return Optional.empty();
        }
    }

    // Créer une question ouverte en fonction d'un film
    private Optional<OpenQuestion> createdOpenQuestionImdb(String inputMovie) {
        // 1 - Convertir la string en Optional<MovieInfo>
        Optional<MovieInfo> movie = getMovieInfo(inputMovie);

        // 2 - Créer la question
        int rand = new Random().nextInt(4);

        Optional<OpenQuestion> question = Optional.empty();

        while(question.isEmpty()) {
            switch(rand) {
                case 0:
                    question = movie.map(movieInfo -> getOpenQuestion().movieInfoTo3actors(movieInfo));
                    break;
                case 1:
                    question = movie.map(movieInfo -> getOpenQuestion().movieInfoToActorName(movieInfo));
                    break;
                case 2:
                    question = movie.map(movieInfo -> getOpenQuestion().movieInfoToMainCharacter(movieInfo));
                    break;
                case 3:
                    question = movie.map(movieInfo -> getOpenQuestion().goodYear(movieInfo));
                    break;
                default: question =  Optional.empty();
            }
        } return question;
    }

    // Créer une question QCM en fonction d'un film
    private Optional<QcmQuestion> createdQcmQuestionImdb(String inputMovie) {
        // 1 - Convertir la string en Optional<MovieInfo>
        Optional<MovieInfo> movie = getMovieInfo(inputMovie);

        // 2 - Créer la question

        int rand = new Random().nextInt(3);

        Optional<QcmQuestion> question = Optional.empty();

        while (question.isEmpty()) {
            switch(rand) {
                case 0:
                    question = movie.map(movieInfo -> getQcmQuestion().goodActorInAMovie(movieInfo));
                    break;
                case 1:
                    question = movie.map(movieInfo -> getQcmQuestion().goodMovie(movieInfo));
                    break;
                case 2:
                    question = movie.map(movieInfo -> getQcmQuestion().goodYearInAMovie(movieInfo));
                    break;
                default: question = Optional.empty();
            }
        } return question;
    }

    // Créer une question True/False en fonction d'un film
    private Optional<TrueFalseQuestion> createdTrueFalseQuestionImdb(String inputMovie) {
        // 1 - Convertir la string en Optional<MovieInfo>
        Optional<MovieInfo> movie = getMovieInfo(inputMovie);

        // 2 - Créer la question

        int rand = new Random().nextInt(6);

        Optional<TrueFalseQuestion> question = Optional.empty();

        while (question.isEmpty())
        switch(rand) {
            case 0:
                question = movie.map(movieInfo -> getTFQuestion().goodActor(movieInfo));
                break;
            case 1:
                question = movie.map(movieInfo -> getTFQuestion().wrongActor(movieInfo));
                break;
            case 2:
                question = movie.map(movieInfo -> getTFQuestion().goodMovie(movieInfo));
                break;
            case 3:
                question = movie.map(movieInfo -> getTFQuestion().wrongMovie(movieInfo));
                break;
            case 4:
                question = movie.map(movieInfo -> getTFQuestion().goodYear(movieInfo));
                break;
            case 5:
                question = movie.map(movieInfo -> getTFQuestion().wrongYear(movieInfo));
                break;

            default: return Optional.empty();
        } return question;
    }

}
