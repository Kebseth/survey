package root.imdb;

public enum Difficulties {
    EASY,
    MEDIUM,
    HARD;
}
